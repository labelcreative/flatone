                <?php wp_footer(); ?>
                <?php tfo('closing_body_code'); ?>
            </div>
        </div>
        <footer class="footer p1 text-center" id="footer" role="contentinfo">
            <img class="footer-logo" src="<?php the_field('site_logo_footer','option'); ?>" alt="Flat One">
        </footer>
    </body>
</html> <?php // end page. what a ride! ?>