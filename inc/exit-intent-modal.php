
<div id="ouibounce-modal" class="ouibounce-modal">
    <div class="underlay"></div>
    <div class="modal">
    	<?php acf_image('modal_image', 'ma'); ?>
        <div class="cf p1  top-form-wrap">
			<?php 
				// if (get_field('button_or_shortcode') == 'Shortcode'): ?>
				<div class="email-wrap">
					<?php echo do_shortcode('[gravityform id=1]'); ?>
				</div><?
				// else:
					?><!-- <a href=" ?><?php tf('button_link'); ?>" class="<?php tf('button_class'); ?>"> <?php tf('button_text'); ?> </a>  --><?php
				// endif;
			?>
		</div>
		<p class="text-center modal-headline ma p1 pt0 "><?php tf('modal_headline'); ?></p>
        <div class="modal-dismiss popup-modal-dismiss tdu italic"><?php tf('dismiss_text'); ?></div>
    </div>
</div>
<script>
jQuery(document).ready(function($) {
	// ouibounce - the plugin is in the head
	var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'), {
		aggressive: true,
		timer: 0,
	});
	$('#ouibounce-modal .popup-modal-dismiss').on('click', function() {
        $('#ouibounce-modal').hide();
    });
    $('#ouibounce-modal .modal').on('click', function(e) {
        e.stopPropagation();
    });
});
</script>