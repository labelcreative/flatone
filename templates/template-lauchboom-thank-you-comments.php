<?php 
/*
Template Name: Thank You w/Comments
*/

    get_header(); 
?>
<?php //acf_image('logo', 'ma from-m-down logo-mobile'); ?>
<?php acf_image('image_mobile', 'from-m-down'); ?>
<div class="cf bgi thank-you-wrap"> 
    <div class="mw-960 p1 pb0">
        <div class="cf">
            <div class="top-content-inner top-content-inner--comments ninecol first">
                <div class="normal-page"><?php tf('text_1'); ?></div>
                <div class="thank-you-like"><?php tf('like_box'); ?></div>
                <div class="normal-page"><?php tf('text_2'); ?></div>
                <div class="cf thank-you-buttons--comments">
                    <a href="#" class="button thank-you-button mt05 facebook"><?php tf('fb_button_text'); ?></a>
                    <a href="#" class="button thank-you-button mt05 twitter"><?php tf('tw_button_text'); ?></a>
                </div>
                <div class="normal-page"><?php tf('text_3'); ?></div>
            </div>   
            <div class="top-content-image--comments threecol last from-m-up mt2">
                <?php acf_image('image'); ?>
            </div>     
        </div>
    </div>
    <div class="text-center p1 mw-710"><?php tf('comments_script'); ?></div>
</div>

<script>
    jQuery(document).ready(function($) {
        $(function() {
            $('.facebook').on('click', function(e) {
                e.preventDefault();
                var w = 580, h = 300,
                        left = (screen.width/2)-(w/2),
                        top = (screen.height/2)-(h/2);
                    
                    
                    if ((screen.width < 480) || (screen.height < 480)) {
                        window.open ('http://www.facebook.com/share.php?u=<?php the_field('fb_share_link'); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
                    } else {
                        window.open ('http://www.facebook.com/share.php?u=<?php the_field('fb_share_link'); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);   
                    }
            });
            
            $('.twitter').on('click', function(e) {
                e.preventDefault();
                var loc = encodeURIComponent('<?php the_field('tw_share_link'); ?>'),
                        title = "<?php the_field('tw_share_text'); ?>",
                        w = 580, h = 300,
                        left = (screen.width/2)-(w/2),
                        top = (screen.height/2)-(h/2);
                        
                    window.open('http://twitter.com/share?text=' + title + '&url=' + loc, '', 'height=' + h + ', width=' + w + ', top='+top +', left='+ left +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
            });
            
            $('.email').on('click', function(e) {
                e.preventDefault();
                var subject = '<?php the_field('email_subject'); ?>';
                var emailBody = "<?php the_field('email_text'); ?>";
                window.location = 'mailto:?subject=' + subject + '&body=' +   emailBody;
            });
        });
    });
</script>
<?php get_footer(); ?>