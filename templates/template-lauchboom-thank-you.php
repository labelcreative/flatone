<?php 
/*
Template Name: Thank You
*/

	get_header(); 
?>
<section class="home-section thanks-page">
    <div class="container mw-1250">

        <div class="thanks-content">
            <div class="text-column">
                <div class="text-center">
                    <a class="site-logo" href="/">
                        <img src="<?php the_field('site_logo_white','option'); ?>" alt="Flat One">
                    </a>
                </div>
                <div class="text-editor">
                    <?php tf('thanks_content'); ?>
                </div>
                <div class="cf thank-you-buttons">
                    <a href="#" class="button thank-you-button mt05 facebook"><?php tf('fb_button_text'); ?></a>
                    <a href="#" class="button thank-you-button mt05 twitter"><?php tf('tw_button_text'); ?></a>
                    <a href="#" class="button thank-you-button mt05 email white"><?php tf('email_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    jQuery(document).ready(function($) {
        $(function() {
            $('.facebook').on('click', function(e) {
                e.preventDefault();
                var w = 580, h = 300,
                        left = (screen.width/2)-(w/2),
                        top = (screen.height/2)-(h/2);
                    
                    
                    if ((screen.width < 480) || (screen.height < 480)) {
                        window.open ('http://www.facebook.com/share.php?u=<?php the_field('fb_share_link'); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
                    } else {
                        window.open ('http://www.facebook.com/share.php?u=<?php the_field('fb_share_link'); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);   
                    }
            });
            
            $('.twitter').on('click', function(e) {
                e.preventDefault();
                var loc = encodeURIComponent('<?php the_field('tw_share_link'); ?>'),
                        title = "<?php the_field('tw_share_text'); ?>",
                        w = 580, h = 300,
                        left = (screen.width/2)-(w/2),
                        top = (screen.height/2)-(h/2);
                        
                    window.open('http://twitter.com/share?text=' + title + '&url=' + loc, '', 'height=' + h + ', width=' + w + ', top='+top +', left='+ left +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
            });
            
            $('.email').on('click', function(e) {
                e.preventDefault();
                var subject = '<?php the_field('email_subject'); ?>';
                var emailBody = "<?php the_field('email_text'); ?>";
                window.location = 'mailto:?subject=' + subject + '&body=' +   emailBody;
            });
        });
    });
</script>
<?php get_footer(); ?>