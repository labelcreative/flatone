<?php 
/*
Template Name: Launchboom Home
*/
	get_header(); 
?>

<section class="home-section">
	<div class="container mw-1250">

		<div class="home-content">
			<div class="text-center">
				<a class="site-logo" href="/">
					<img src="<?php the_field('site_logo_dark','option'); ?>" alt="Flat One">
				</a>
			</div>
			<div class="column">
				<h1><?php tf('landing_title'); ?></h1>
				<div class="img-holder">
					<img src="<?php tf('landing_image'); ?>" alt="Flat One">
				</div>
				<div class="text"><?php tf('landing_head_text'); ?></div>
				<div class="form-holder">
					<div class="form-text hidden-tablet"><?php tf('form_heading'); ?></div>
					<div class="text-editor"><?php tf('bottom_content'); ?></div>
				</div>
			</div>
			<div class="visible-tablet bottom-content">
				<div class="form-text"><?php tf('form_heading'); ?></div>
				<div class="image">
					<img src="<?php tf('landing_image_bottom_mobile'); ?>" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_template_part('inc/exit-intent-modal'); ?>

<?php get_footer(); ?>